INSERT INTO region (name) VALUES ('North America')
INSERT INTO region (name) VALUES ('Europe')
INSERT INTO region (name) VALUES ('Asia')

INSERT INTO community (region_id, name) VALUES (1, 'New York')
INSERT INTO community (region_id, name) VALUES (1, 'Los Angeles')
INSERT INTO community (region_id, name) VALUES (1, 'Chicago')
INSERT INTO community (region_id, name) VALUES (2, 'London')
INSERT INTO community (region_id, name) VALUES (2, 'Paris')
INSERT INTO community (region_id, name) VALUES (2, 'Berlin')
INSERT INTO community (region_id, name) VALUES (3, 'Tokyo')
INSERT INTO community (region_id, name) VALUES (3, 'Beijing')
INSERT INTO community (region_id, name) VALUES (3, 'Seoul')

INSERT INTO product (name) VALUES ('Apples')
INSERT INTO product (name) VALUES ('Bananas')
INSERT INTO product (name) VALUES ('Oranges')
INSERT INTO product (name) VALUES ('Pears')
INSERT INTO product (name) VALUES ('Grapes')
INSERT INTO product (name) VALUES ('Strawberries')
INSERT INTO product (name) VALUES ('Blueberries')
INSERT INTO product (name) VALUES ('Raspberries')
INSERT INTO product (name) VALUES ('Blackberries')
INSERT INTO product (name) VALUES ('Pineapples')
INSERT INTO product (name) VALUES ('Mangoes')
INSERT INTO product (name) VALUES ('Papayas')
INSERT INTO product (name) VALUES ('Watermelons')
INSERT INTO product (name) VALUES ('Cantaloupes')
INSERT INTO product (name) VALUES ('Honeydews')
INSERT INTO product (name) VALUES ('Lemons')
INSERT INTO product (name) VALUES ('Limes')
INSERT INTO product (name) VALUES ('Grapefruits')

INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (1, 1, 100, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (1, 2, 200, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (1, 3, 300, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (1, 4, 400, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (1, 5, 500, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (2, 1, 100, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (2, 2, 200, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (2, 3, 300, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (2, 4, 400, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (2, 5, 500, '2020-01-01')
INSERT INTO consumption (community_id, product_id, quantity, date) VALUES (3, 1, 100, '2020-01-01')

INSERT INTO waste (community_id, product_id, quantity) VALUES (1, 1, 10)
INSERT INTO waste (community_id, product_id, quantity) VALUES (1, 2, 20)
INSERT INTO waste (community_id, product_id, quantity) VALUES (1, 3, 30)
INSERT INTO waste (community_id, product_id, quantity) VALUES (1, 4, 40)
INSERT INTO waste (community_id, product_id, quantity) VALUES (1, 5, 50)
INSERT INTO waste (community_id, product_id, quantity) VALUES (2, 1, 10)
INSERT INTO waste (community_id, product_id, quantity) VALUES (2, 2, 20)
INSERT INTO waste (community_id, product_id, quantity) VALUES (2, 3, 30)
INSERT INTO waste (community_id, product_id, quantity) VALUES (2, 4, 40)
INSERT INTO waste (community_id, product_id, quantity) VALUES (2, 5, 50)
INSERT INTO waste (community_id, product_id, quantity) VALUES (3, 1, 10)

UPDATE `community` SET `population` = 8468000 WHERE `community_id` = 1;
UPDATE `community` SET `population` = 3849000 WHERE `community_id` = 2;
UPDATE `community` SET `population` = 2697000 WHERE `community_id` = 3;
UPDATE `community` SET `population` = 8908081 WHERE `community_id` = 4;
UPDATE `community` SET `population` = 2140526 WHERE `community_id` = 5;
UPDATE `community` SET `population` = 3769495 WHERE `community_id` = 6;
UPDATE `community` SET `population` = 13929286 WHERE `community_id` = 7;
UPDATE `community` SET `population` = 21542000 WHERE `community_id` = 8;
UPDATE `community` SET `population` = 21710000 WHERE `community_id` = 9;