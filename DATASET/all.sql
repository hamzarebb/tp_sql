SELECT 
    c.name AS community_name,
    r.name AS region_name,
    p.name AS product_name,
    co.quantity AS consumption_quantity,
    co.date AS consumption_date,
    w.quantity AS waste_quantity
FROM 
    community c
INNER JOIN 
    region r ON c.region_id = r.region_id
INNER JOIN 
    consumption co ON c.community_id = co.community_id
INNER JOIN 
    product p ON co.product_id = p.product_id
LEFT JOIN 
    waste w ON c.community_id = w.community_id AND co.product_id = w.product_id