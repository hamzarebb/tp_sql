/*
La liste des 50 produits les plus achetés sur un mois donné, 
en ordonnant le résultat dans l’ordre décroissant des quantités achetées
*/

SELECT p.name AS product_name, SUM(c.quantity) AS total_quantity
FROM consumption c
JOIN product p ON c.product_id = p.product_id
WHERE MONTH(c.date) = 1 AND YEAR(c.date) = 2020
GROUP BY p.name
ORDER BY total_quantity DESC
LIMIT 50