/*
La valeur du stock moyen toute communauté confondue
*/

SELECT AVG(stock_value)
FROM (
    SELECT SUM(c.quantity) - COALESCE(SUM(w.quantity), 0) AS stock_value
    FROM consumption c
    LEFT JOIN waste w ON c.product_id = w.product_id
    GROUP BY c.product_id
) AS stock_values
