/*
La liste des 50 produits les plus gaspillés toute période confondue
*/

SELECT p.name AS product_name, SUM(w.quantity) AS total_waste
FROM waste w
JOIN product p ON w.product_id = p.product_id
GROUP BY p.name
ORDER BY total_waste DESC
LIMIT 50