/*
La valeur du stock moyen répartie par région
*/

SELECT r.name AS region_name,
AVG(co.quantity) AS average_stock_value
FROM Region r
JOIN Community c ON r.region_id = c.region_id
JOIN Consumption co ON c.community_id = co.community_id
GROUP BY r.name
LIMIT 50