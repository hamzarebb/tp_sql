/*
Le nombre de personnes dans les « communautés » trié dans l’ordre
décroissant de ce nombre,
*/

SELECT c.name AS community_name, SUM(c.population) AS groupement
FROM Community c
JOIN Consumption cs ON c.community_id = cs.community_id
GROUP BY c.name
ORDER BY groupement DESC