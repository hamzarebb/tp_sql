/*
La liste des produits les plus gaspillés regroupée par composition des communautés
*/

SELECT p.name AS product_name, c.name AS community_name, SUM(w.quantity) AS total_waste
FROM waste w
JOIN product p ON w.product_id = p.product_id
JOIN community c ON w.community_id = c.community_id
GROUP BY p.name, c.name
ORDER BY total_waste DESC
LIMIT 50