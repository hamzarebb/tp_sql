/*
Le nombre de personnes dans les « communautés » réparti par régions, 
trié par région puis par nombre de personnes
*/

SELECT r.name AS region_name, c.name AS community_name, SUM(c.population) AS total_members
FROM Community c
JOIN Region r ON c.region_id = r.region_id
JOIN Consumption cs ON c.community_id = cs.community_id
GROUP BY r.name, c.name
ORDER BY r.name, total_members DESC