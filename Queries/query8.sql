/*
La valeur du stock moyen répartie en fonction de la composition des
communautés
*/
SELECT c.name AS community_name, 
AVG(co.quantity) AS average_stock_value
FROM Community c
JOIN Consumption co ON c.community_id = co.community_id
GROUP BY c.name