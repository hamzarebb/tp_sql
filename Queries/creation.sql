CREATE TABLE `community` (
	`community_id` int NOT NULL AUTO_INCREMENT,
	`region_id` int NOT NULL,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`community_id`)
)

CREATE TABLE `region` (
	`region_id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`region_id`)
)

CREATE TABLE `product` (
	`product_id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`product_id`)
)

CREATE TABLE `consumption` (
	`consumption_id` int NOT NULL AUTO_INCREMENT,
	`community_id` int NOT NULL,
	`product_id` int NOT NULL,
	`quantity` int NOT NULL,
	`date` DATE NOT NULL,
	PRIMARY KEY (`consumption_id`)
)

CREATE TABLE `waste` (
	`waste_id` int NOT NULL AUTO_INCREMENT,
	`community_id` int NOT NULL,
	`product_id` int NOT NULL,
	`quantity` int NOT NULL,
	PRIMARY KEY (`waste_id`)
)

ALTER TABLE `community` ADD CONSTRAINT `community_fk0` FOREIGN KEY (`region_id`) REFERENCES `region`(`region_id`)

ALTER TABLE `consumption` ADD CONSTRAINT `consumption_fk0` FOREIGN KEY (`community_id`) REFERENCES `community`(`community_id`)

ALTER TABLE `consumption` ADD CONSTRAINT `consumption_fk1` FOREIGN KEY (`product_id`) REFERENCES `product`(`product_id`)

ALTER TABLE `waste` ADD CONSTRAINT `waste_fk0` FOREIGN KEY (`community_id`) REFERENCES `community`(`community_id`)

ALTER TABLE `waste` ADD CONSTRAINT `waste_fk1` FOREIGN KEY (`product_id`) REFERENCES `product`(`product_id`)

ALTER TABLE `community` ADD COLUMN `population` int NOT NULL DEFAULT 0