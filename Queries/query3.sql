/*
La liste des 50 produits les plus consommés 
sur un mois donné en ordonnant le résultat dans l’ordre décroissant des consommations
*/

SELECT p.name AS product_name, SUM(cs.quantity) AS total_consumption
FROM Product p
JOIN Consumption cs ON p.product_id = cs.product_id
WHERE MONTH(cs.date) = 01 AND YEAR(cs.date) = 2020
GROUP BY p.name
ORDER BY total_consumption DESC
LIMIT 50